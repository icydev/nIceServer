#pragma once
#include "Html/Element.h"

namespace Templates {
	class DocumentTemplate {
	public:
		Html::ElementList& Head();
		Html::ElementList& Body();

		std::string ToString() const;

	private:
		Html::ElementList head;
		Html::ElementList body;
	};
}
