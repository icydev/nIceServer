#include "DocumentTemplate.h"

using namespace std;
using namespace Html;

namespace Templates {
	ElementList& DocumentTemplate::Head() {
		return head;
	}

	ElementList& DocumentTemplate::Body() {
		return body;
	}

	string DocumentTemplate::ToString() const {
		string headstr;
		for (size_t i = 0; i < head.size(); i++) {
			headstr += head[i]->ToString();
		}
		string bodystr;
		for (size_t i = 0; i < body.size(); i++) {
			bodystr += body[i]->ToString();
		}

		return string("<!DOCTYPE html><html><head><meta charset=\"UTF - 8\">") + headstr + "</head><body>" + bodystr + "</body></html>";
	}
}
