#include "Element.h"

using namespace std;

namespace Html {
	vector<Element>& Element::Children() {
		return children;
	}

	string Element::ChildrenToString() const {
		string ret;
		for (size_t i = 0; i < children.size(); i++) {
			ret += children[i].ChildrenToString();
		}
		return ret;
	}
}