#pragma once
#include <memory>
#include <string>
#include <vector>

namespace Html {
	class Element {
	public:
		std::vector<Element>& Children();
		virtual std::string ToString() const = 0;

	protected:
		std::string ChildrenToString() const;

	private:
		std::vector<Element> children;
	};

	using ElementList = std::vector<std::unique_ptr<Html::Element>>;
}
