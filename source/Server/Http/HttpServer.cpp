#include "HttpServer.h"
#include "Server/SocketServer.h"
#include "Util/Set.h"
#include "Util/String.h"

using namespace std;
using namespace Util;

namespace Server {
	namespace Http {
		namespace HttpServer {
			unordered_map<string, RouteNode> routes;

			RouteNode* findRoute(const std::string& method, const std::string& path) {
				unordered_map<string, RouteNode>::iterator routeiter = routes.find(method);
				if (routeiter == routes.end()) {
					return nullptr;
				}

				RouteNode& subroute = routeiter->second;
				vector<string> pathsplit = String::Split(path, '/');
				for (size_t i = 1; i < pathsplit.size(); i++) {
					routeiter = subroute.Find(pathsplit[i]);
					if (routeiter == subroute.End()) {
						if (i == pathsplit.size() - 1 && pathsplit[i] == "") {
							break;
						} else {
							return nullptr;
						}
					}
					subroute = routeiter->second;
				}

				return &subroute;
			}

			void Run() {
				SocketServer::Run();
				/*SocketServer::Run([](ClientSocket& client) {
					HttpRequest request;
					request.Method = client.ReadWord(7);
					request.Path = client.ReadWord(256);

					if (client.ReadWord(8) != "HTTP/1.1") {
						return;
					}

					while (true) {
						string headerline = client.ReadLine(256);
						if (String::IsWhitespace(headerline)) {
							break;
						}

						vector<string> headersplit = String::Split(headerline, ':', 2);
						if (headersplit.size() != 2) {
							return;
						}
						request.Headers[headersplit[0]] = move(headersplit[1]);
					}

					RouteNode* route = findRoute(request.Method, request.Path);
					if (route == nullptr) {
						return;
					}

					route->Action()(request);
					client.SendText(request.Method + " ");
					client.SendText(request.Path + " HTTP/1.1\n");
				});*/
			}

			RouteNode& Routes(const string& method) {
				return routes[method];
			}
		}
	}
}
