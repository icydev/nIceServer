#include "RouteNode.h"

using namespace std;

namespace Server {
	namespace Http {
		RouteNode& RouteNode::operator[](const string& subpath) {
			return subroutes[subpath];
		}

		RouteNode::ActionFunc RouteNode::Action() {
			return action;
		}

		void RouteNode::Action(ActionFunc action) {
			this->action = action;
		}

		RouteNode::RouteMap::iterator RouteNode::Begin() {
			return subroutes.begin();
		}

		RouteNode::RouteMap::iterator RouteNode::End() {
			return subroutes.end();
		}

		RouteNode::RouteMap::iterator RouteNode::Find(const string &keyval) {
			return subroutes.find(keyval);
		}
	}
}
