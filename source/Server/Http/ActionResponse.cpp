#include "ActionResponse.h"

namespace Server {
	namespace Http {
		ActionResponse ActionResponse::Html(std::string &&html) {
			ActionResponse ret;
			ret.response = move(html);
			return ret;
		}
	}
}
