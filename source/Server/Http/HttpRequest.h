#pragma once
#include <string>
#include <unordered_map>

namespace Server {
	namespace Http {
		struct HttpRequest {
			std::string Method;
			std::string Path;
			std::unordered_map<std::string, std::string> Headers;
		};
	}
}
