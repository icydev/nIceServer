#pragma once
#include <string>

namespace Server {
	namespace Http {
		class ActionResponse {
		public:
			static ActionResponse Html(std::string&& html);

		private:
			std::string response;
		};
	}
}