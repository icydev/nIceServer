#pragma once
#include "RouteNode.h"

namespace Server {
	namespace Http {
		namespace HttpServer {
			void Run();
			RouteNode& Routes(const std::string& method);
		}
	}
}
