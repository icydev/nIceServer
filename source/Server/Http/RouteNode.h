#pragma once
#include <functional>
#include <unordered_map>
#include "ActionResponse.h"
#include "HttpRequest.h"

namespace Server {
	namespace Http {
		class RouteNode {
		public:
			using RouteMap = std::unordered_map<std::string, RouteNode>;
			using ActionFunc = std::function<ActionResponse(const HttpRequest&)>;

			ActionFunc Action();
			void Action(ActionFunc action);

			RouteMap::iterator Begin();
			RouteMap::iterator End();

			RouteMap::iterator Find(const std::string &keyval);

			RouteNode& operator[](const std::string& subpath);

		private:
			RouteMap subroutes;
			ActionFunc action;
		};
	}
}
