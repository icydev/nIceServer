#include "Socket.h"
#include "SocketOverlapped.h"
#include <MSWSock.h>
#include <stdexcept>
#include <string>

using namespace std;

namespace Server
{
	namespace ServerPrivate
	{
		LPFN_ACCEPTEX AcceptEx = nullptr;
	}

#define P ServerPrivate

	BufferRef::BufferRef(char* buf, unsigned long len) {
		wsabuf.buf = buf;
		wsabuf.len = len;
	}

	Socket::Socket(AddressFamily family, SocketType type, Protocol protocol) {
		handle = WSASocket((int)family, (int)type, (int)protocol, NULL, 0, WSA_FLAG_OVERLAPPED);
		if (handle == INVALID_SOCKET) {
			throw runtime_error("WSASocket() failed: " + to_string(WSAGetLastError()));
		}
	}

	Socket::Socket(Socket&& src) : handle(src.handle) {
		src.handle = INVALID_SOCKET;
	}

	Socket::~Socket() {
		if (handle != INVALID_SOCKET) {
			closesocket(handle);
		}
	}

	SOCKET Socket::Handle() const {
		return handle;
	}

	Socket Socket::AcceptEx(SocketOverlapped& overlapped) {
		const int addrlen = sizeof(sockaddr_in) + 16;
		static char acceptbuf[addrlen * 2];
		static DWORD bytes;

		if (P::AcceptEx == nullptr) {
			GUID guidAcceptEx = WSAID_ACCEPTEX;
			if (WSAIoctl(handle, SIO_GET_EXTENSION_FUNCTION_POINTER, &guidAcceptEx, sizeof(guidAcceptEx), &P::AcceptEx, sizeof(P::AcceptEx), &bytes, nullptr, nullptr) == SOCKET_ERROR) {
				throw runtime_error("WSAIoctl() failed: " + to_string(WSAGetLastError()));
			}
		}

		Socket acceptsock(AddressFamily::Inet, SocketType::Stream, Protocol::Tcp);

		if (P::AcceptEx(handle, acceptsock.Handle(), acceptbuf, 0, addrlen, addrlen, &bytes, &overlapped.Overlapped())) {
			int err = WSAGetLastError();
			if (err != ERROR_IO_PENDING) {
				throw runtime_error("AcceptEx() failed: " + to_string(WSAGetLastError()));
			}
		}

		return acceptsock;
	}

	void Socket::Bind(AddressFamily family, unsigned short port) {
		sockaddr_in inaddr;
		inaddr.sin_family = (ADDRESS_FAMILY)family;
		inaddr.sin_addr.s_addr = htonl(INADDR_ANY);
		inaddr.sin_port = htons(port);

		if (bind(handle, reinterpret_cast<sockaddr*>(&inaddr), sizeof(inaddr)) == SOCKET_ERROR) {
			throw runtime_error("bind() failed: " + to_string(WSAGetLastError()));
		}
	}

	void Socket::Listen(int backlog) {
		if (listen(handle, backlog) == SOCKET_ERROR) {
			throw runtime_error("listen() failed: " + to_string(WSAGetLastError()));
		}
	}

	void Socket::Recv(vector<BufferRef> bufs, SocketOverlapped& overlapped) {
		DWORD flags = 0;
		if (WSARecv(handle, reinterpret_cast<WSABUF*>(bufs.data()), (DWORD)bufs.size(), NULL, &flags, &overlapped.Overlapped(), NULL) == SOCKET_ERROR) {
			if (WSAGetLastError() != ERROR_IO_PENDING) {
				int err = WSAGetLastError();
				throw runtime_error("WSARecv() failed: " + to_string(err));
			}
		}
	}

	void Socket::Shutdown(ShutdownHow how) {
		if (shutdown(handle, (int)how) == SOCKET_ERROR) {
			int err = WSAGetLastError();
			throw runtime_error("shutdown() failed: " + to_string(err));
		}
	}

	Socket& Socket::operator=(Socket&& rhs) {
		if (handle != rhs.handle) {
			this->~Socket();
			handle = rhs.handle;
			rhs.handle = INVALID_SOCKET;
		}
		return *this;
	}
}
