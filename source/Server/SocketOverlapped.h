#pragma once
#include "Socket.h"
#include "Event.h"
#include <memory>

namespace Server
{
	class SocketOverlapped
	{
	public:
		SocketOverlapped(const Socket& socket, const Event& event);

		WSAOVERLAPPED& Overlapped();

		bool GetResult(DWORD& transfer, DWORD& flags, bool wait = false);

	private:
		std::unique_ptr<WSAOVERLAPPED> overlapped;
		SOCKET socket;
	};
}
