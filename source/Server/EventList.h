#pragma once
#include "Event.h"
#include <vector>

namespace Server
{
	class EventList
	{
	public:
		Event& Back();
		const Event& Back() const;
		void Erase(size_t index);
		size_t Size() const;

		void AddEvent();
		size_t Wait(bool waitall);

		Event& operator[](size_t index);
		const Event& operator[](size_t index) const;

	private:
		std::vector<Event> events;
	};
}
