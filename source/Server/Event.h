#pragma once
#include <winsock2.h>

namespace Server
{
	class Event
	{
	public:
		Event();
		Event(const Event&) = delete;
		Event(Event&& src);
		~Event();

		WSAEVENT Handle() const;

		void Reset();

		Event& operator=(Event&& rhs);

	private:
		WSAEVENT handle;
	};
}
