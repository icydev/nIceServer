#include "EventList.h"
#include <stdexcept>
#include <string>

using namespace std;

namespace Server
{
	Event& EventList::Back() {
		return events.back();
	}

	const Event& EventList::Back() const {
		return events.back();
	}

	void EventList::Erase(size_t index) {
		events.erase(events.begin() + index);
	}

	size_t EventList::Size() const {
		return events.size();
	}

	void EventList::AddEvent() {
		events.push_back(Event());
	}

	size_t EventList::Wait(bool waitall) {
		DWORD waitret = WSAWaitForMultipleEvents((DWORD)events.size(), reinterpret_cast<WSAEVENT*>(events.data()), waitall, WSA_INFINITE, FALSE);
		if (waitret == WSA_WAIT_FAILED) {
			int err = WSAGetLastError();
			throw runtime_error("WSAWaitForMultipleEvents() failed: " + to_string(err));
		}
		return (size_t)(waitret - WSA_WAIT_EVENT_0);
	}

	Event& EventList::operator[](size_t index) {
		return events[index];
	}

	const Event& EventList::operator[](size_t index) const  {
		return events[index];
	}
}
