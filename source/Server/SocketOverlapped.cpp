#include "SocketOverlapped.h"

namespace Server
{
	SocketOverlapped::SocketOverlapped(const Socket& socket, const Event& event) : overlapped(new WSAOVERLAPPED()), socket(socket.Handle()) {
		memset(overlapped.get(), 0, sizeof(WSAOVERLAPPED));
		overlapped->hEvent = event.Handle();
	}

	WSAOVERLAPPED& SocketOverlapped::Overlapped() {
		return *overlapped;
	}

	bool SocketOverlapped::GetResult(DWORD& transfer, DWORD& flags, bool wait) {
		return WSAGetOverlappedResult(socket, overlapped.get(), &transfer, wait, &flags);
	}
}
