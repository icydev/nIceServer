#include "SocketServer.h"
#include "SocketOverlapped.h"
#include "EventList.h"
#ifdef _WIN32
#include <ws2tcpip.h>
#include <MSWSock.h>
#endif
#include <string>
#include <mutex>
#include <future>

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

namespace Server {
	namespace SocketServer {
		void startup() {
			WSADATA wsaData;
			int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
			if (result != 0) {
				throw runtime_error("WSAStartup() failed: " + to_string(result));
			}
		}

		void Run() {
			const unsigned short PORT = 8080;

			startup();

			EventList events;
			events.AddEvent();

			vector<Socket> sockets;
			sockets.push_back(Socket(AddressFamily::Inet, SocketType::Stream, Protocol::Tcp));
			sockets[0].Bind(AddressFamily::Inet, PORT);
			sockets[0].Listen();

			vector<SocketOverlapped> overlaps;
			overlaps.push_back(SocketOverlapped(sockets[0], events[0]));

			vector<vector<char>> buffers;

			sockets.push_back(sockets[0].AcceptEx(overlaps[0]));

			while (true)
			{
				size_t index = events.Wait(false);
				events[index].Reset();

				DWORD bytes;
				DWORD flags;
				if (overlaps[index].GetResult(bytes, flags)) {
					if (index == 0) {
						events.AddEvent();
						overlaps.push_back(SocketOverlapped(sockets.back(), events.Back()));
						buffers.push_back(vector<char>(256));

						sockets.back().Recv({ BufferRef(buffers.back().data(), 256) }, overlaps.back());

						sockets.push_back(sockets[0].AcceptEx(overlaps[0]));
					} else if (bytes == 256) {
						size_t datalen = buffers[index - 1].size();
						buffers[index - 1].resize(datalen + 256);
						sockets[index].Recv({ BufferRef(&buffers[index - 1][datalen], 256) }, overlaps[index]);
					} else {
						buffers[index - 1].resize(buffers[index - 1].size() - 256 + bytes);

						sockets.erase(sockets.begin() + index);
						overlaps.erase(overlaps.begin() + index);
						buffers.erase(buffers.begin() + index - 1);
						events.Erase(index);
					}
				} else {
					if (index == 0) {
						int err = WSAGetLastError();
						throw runtime_error("AcceptEx() failed: " + to_string(err));
					} else {
						int err = WSAGetLastError();
						throw runtime_error("Recv() failed: " + to_string(err));

						sockets.erase(sockets.begin() + index);
						overlaps.erase(overlaps.begin() + index);
						buffers.erase(buffers.begin() + index - 1);
						events.Erase(index);
					}
				}
			}
		}
	}
}
