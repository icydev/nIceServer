#include "Event.h"
#include <stdexcept>
#include <string>

using namespace std;

namespace Server
{
	Event::Event() {
		handle = WSACreateEvent();
		if (handle  == WSA_INVALID_EVENT) {
			throw runtime_error("WSACreateEvent() failed: " + to_string(WSAGetLastError()));
		}
	}

	Event::Event(Event&& src) : handle(src.handle) {
		src.handle = WSA_INVALID_EVENT;
	}

	Event::~Event() {
		if (handle != WSA_INVALID_EVENT) {
			WSACloseEvent(handle);
		}
	}

	WSAEVENT Event::Handle() const {
		return handle;
	}

	void Event::Reset() {
		if (!WSAResetEvent(handle)) {
			throw runtime_error("WSAResetEvent() failed: " + to_string(WSAGetLastError()));
		}
	}

	Event& Event::operator=(Event&& rhs) {
		if (handle != rhs.handle) {
			this->~Event();
			handle = rhs.handle;
			rhs.handle = WSA_INVALID_EVENT;
		}
		return *this;
	}
}
