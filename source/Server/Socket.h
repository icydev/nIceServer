#pragma once
#include <winsock2.h>
#include <vector>

namespace Server
{
	enum class AddressFamily : unsigned short
	{
		Unspec = 0,			// unspecified
		Unix = 1,			// local to host (pipes, portals)
		Inet = 2,			// internetwork: UDP, TCP, etc.
		Implink = 3,		// arpanet imp addresses
		Pup = 4,			// pup protocols: e.g. BSP
		Chaos = 5,			// mit CHAOS protocols
		Ns = 6,				// XEROX NS protocols
		Ipx = Ns,			// IPX protocols: IPX, SPX, etc.
		Iso = 7,			// ISO protocols
		Osi = Iso,			// OSI is ISO
		Ecma = 8,			// european computer manufacturers
		Datakit = 9,		// datakit protocols
		Ccitt = 10,			// CCITT protocols, X.25 etc
		Sna = 11,			// IBM SNA
		Decnet = 12,		// DECnet
		Dli = 13,			// Direct data link interface
		Lat = 14,			// LAT
		Hylink = 15,		// NSC Hyperchannel
		AppleTalk = 16,		// AppleTalk
		NetBios = 17,		// NetBios-style addresses
		Voiceview = 18,		// VoiceView
		Firefox = 19,		// Protocols from Firefox
		Unknown1 = 20,		// Somebody is using this!
		Ban = 21,			// Banyan
		Atm = 22,			// Native ATM Services
		Inet6 = 23,			// Internetwork Version 6
		Cluster = 24,		// Microsoft Wolfpack
		Ieee12844 = 25,		// IEEE 1284.4 WG AF
		Irda = 26,			// IrDA
		test = 2,			// test
		Netdes = 28,		// Network Designers OSI & gateway
#if(_WIN32_WINNT >= 0x0501)
		TcnProcess = 29,
		TcnMessage = 30,
		Iclfxbm = 31,
#if(_WIN32_WINNT >= 0x0600)
		Bth = 32,			// Bluetooth RFCOMM/L2CAP protocols
#if(_WIN32_WINNT >= 0x0601)
		Link = 33,
#if(_WIN32_WINNT >= 0x0604)
		HyperV = 34,
#endif
#endif
#endif
#endif
	};

	enum class SocketType : int
	{
		Stream = 1,		// stream socket
		Dgram = 2,		// datagram socket
		Raw = 3,		// raw-protocol interface
		Rdm = 4,		// reliably-delivered message
		Seqpacket = 5,	// sequenced packet stream
	};

	enum class Protocol : int
	{
		Icmp = 1,
		Igmp = 2,
		Ggp = 3,
		Tcp = 6,
		Pup = 12,
		Udp = 17,
		Idp = 22,
		Nd = 77,
		Raw = 255,
		Max = 256,
#if(_WIN32_WINNT >= 0x0501)
		Hopopts = 0,	// IPv6 Hop-by-Hop options
		Ipv4 = 4,
		Ipv6 = 41,		// IPv6 header
		Routing = 43,	// IPv6 Routing header
		Fragment = 44,	// IPv6 fragmentation header
		Esp = 50,		// encapsulating security payload
		Ah = 51,		// authentication header
		Icmpv6 = 58,	// ICMPv6
		None = 59,		// IPv6 no next header
		Dstopts = 60,	// IPv6 Destination options
		Iclfxbm = 78,
#if(_WIN32_WINNT >= 0x0600)
		St = 5,
		Cbt = 7,
		Egp = 8,
		Igp = 9,
		Rdp = 27,
		Pim = 103,
		Pgm = 113,
		L2tp = 115,
		Sctp = 132,
#endif
#endif
	};

	enum class ShutdownHow : int
	{
		Receive = 0,
		Send = 1,
		Both = 2
	};

	class BufferRef
	{
	public:
		BufferRef(char* buf, unsigned long len);

	private:
		WSABUF wsabuf;
	};

	class SocketOverlapped;

	class Socket
	{
	public:
		Socket(AddressFamily family, SocketType type, Protocol protocol);
		Socket(const Socket&) = delete;
		Socket(Socket&& src);
		~Socket();

		SOCKET Handle() const;

		Socket AcceptEx(SocketOverlapped& overlapped);
		void Bind(AddressFamily family, unsigned short port);
		void Listen(int backlog = SOMAXCONN);
		void Recv(std::vector<BufferRef> bufs, SocketOverlapped& overlapped);
		void Shutdown(ShutdownHow how);

		Socket& operator=(Socket&& rhs);

	private:
		SOCKET handle;
	};
}
