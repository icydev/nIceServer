#pragma once
#include <vector>

namespace Util {
	template<typename Iter>
	class Enumerable {
	public:
		Enumerable(Iter begin, Iter end) : begin(begin), end(end) {}

		template<typename T>
		bool Contains(const T val) {
			for (Iter iter = begin; iter != end; ++iter) {
				if (val == *iter) {
					return true;
				}
			}
			return false;
		}

	private:
		Iter begin;
		Iter end;
	};

	namespace Set {
		template<typename Iter>
		Enumerable<Iter> From(Iter begin, Iter end) {
			return Enumerable<Iter>(begin, end);
		}

		template<typename T>
		Enumerable<typename std::vector<T>::const_iterator> From(const std::vector<T>& vector) {
			return Enumerable<std::vector<T>::const_iterator>(vector.begin(), vector.end());
		}
	}
}
