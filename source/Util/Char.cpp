#include "Char.h"

namespace Util {
	namespace Char {
		bool IsWhitespace(char ch) {
			return ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t';
		}
	}
}
