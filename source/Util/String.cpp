#include "String.h"
#include <sstream>

using namespace std;

namespace Util {
	namespace String {
		bool IsWhitespace(const string &str) {
			for (size_t i = 0; i < str.size(); i++) {
				if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n' && str[i] != '\r') {
					return false;
				}
			}
			return true;
		}

		string Join(const vector<string>& strs, char sep) {
			size_t retsize = strs.size() - 1;
			for (size_t i = 0; i < strs.size(); i++) {
				retsize += strs[i].size();
			}

			string ret(retsize, 0);
			size_t offset = 0;
			for (size_t i = 0; i < strs.size(); i++) {
				memcpy(&ret[offset], strs[i].data(), strs[i].size());
				offset += strs[i].size();
				ret[offset] = sep;
				offset++;
			}

			return ret;
		}

		vector<string> Split(const string& str, char delim) {
			vector<string> ret;
			size_t offset = 0;
			for (size_t i = 0; i < str.size(); i++) {
				if (str[i] == delim) {
					ret.push_back(str.substr(offset, i - offset));
					offset = i + 1;
				}
			}
			ret.push_back(str.substr(offset, str.size() - offset));
			return ret;
		}

		vector<string> Split(const string& str, char delim, int max) {
			vector<string> ret;
			size_t offset = 0;
			for (size_t i = 0; i < str.size(); i++) {
				if (str[i] == delim) {
					ret.push_back(str.substr(offset, i - offset));
					offset = i + 1;
					if (ret.size() == max - 1) {
						break;
					}
				}
			}
			ret.push_back(str.substr(offset));
			return ret;
		}
	}
}
