#pragma once
#include <vector>
#include <string>

namespace Util {
	namespace String {
		bool IsWhitespace(const std::string& str);
		std::string Join(const std::vector<std::string>& strs, char sep);
		std::vector<std::string> Split(const std::string &str, char delim);
		std::vector<std::string> Split(const std::string &str, char delim, int max);
	}
}
