#pragma once

namespace Util {
	namespace Char {
		bool IsWhitespace(char ch);
	}
}
