#include "Templates/DocumentTemplate.h"
#include "Server/Http/HttpServer.h"
#include <iostream>

using namespace std;
using namespace Templates;
using namespace Server::Http;

int main() {
	HttpServer::Routes("GET").Action([](const HttpRequest& request) { return ActionResponse::Html("test"); });
	HttpServer::Run();
}
